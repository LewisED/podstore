<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true);
define( 'WPCACHEHOME', '/var/www/vhosts/pod-w.co.uk/httpdocs/podstore/wp-content/plugins/wp-super-cache/' );
define( 'DB_NAME', 'podstore' );

/** MySQL database username */
define( 'DB_USER', 'podstore' );

/** MySQL database password */
define( 'DB_PASSWORD', 'EnZxskQ47hb~9hri' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost:3306' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'UOs)B5V83L]@U29~qwM!%M8K89ue[928b4VS7|vt;I(DF3IeFpAU(~8g!fJhX7/d');
define('SECURE_AUTH_KEY', 'f1+2X]6(g#JZ3#6CFWuR09ddI(l8wjL5V/q3E-118@PyV)pqQ7|Pq]2@0v9s5h6V');
define('LOGGED_IN_KEY', 'TI972P-WqA6i3|*sL-Qc2!)7k*nO~4M)S]7/440@(;JFGpg8~SCmSunSy6s5fT2w');
define('NONCE_KEY', '_1G!~cADId5aMj:p2H4*o&15#04o&HXSs|]D6/bvfrXgig3e_j)fF/6N01(&v)&D');
define('AUTH_SALT', '3;3:t~QmFspt!/;@~9rw73(ZVMzyK]8LR0G]39jH62T&mfL&5*B6Zb0o|6UpA5*C');
define('SECURE_AUTH_SALT', 'U5GL#YP51xm0Dr7([#O1)9J1:RL3F~8-sv_EVM33a%LW*m#i_tUs68#fM!|-*]ns');
define('LOGGED_IN_SALT', 'u*K7aj58XeRQD~ESUh&|51&_!64X2kfWx/g1_~~C5Dmfph[rFr5@5H4k5@4+bX4|');
define('NONCE_SALT', '#qrV6O;M_;X)@Z7:42RlN_/zm*p5m1S+[88cSRmu;lr~Sv1k&Yf&hTy_y56e9@UP');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'podst_';


define('WP_ALLOW_MULTISITE', true);
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
